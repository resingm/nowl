
# Nowl

This service helps you to periodically mirror one directory into another one.
It becomes helpful when you easily want to mirror mounted file systems, e.g. an
NFS and mirror it on another NFS mount (or similar setups).

The intention of this software is to simplify backup jobs.
Since the tool is supposed to be executed at night it is called night owl, in short nowl.
It runs, when other services are at sleep.


## Configuration

```
TODO
```


## Setup

```
TODO
```


// standard lib
use std::fs::File;

// 3rd party
use clap::{crate_version, App, Arg};

// 1st party



fn main() {
    let matches = App::new("nowl")
        .version(crate_version!())
        .author("Max Resing <max.resing@protonmail.com>")
        .about("Utility to copy changes-only of a directory. Useful for cron-job backups")
        .arg(Arg::with_name("src")
                .short("s")
                .long("source")
                .value_name("SOURCE")
                .help("Define an input directory as the source of files.")
                .takes_value(true))
        .arg(Arg::with_name("dest")
                .short("d")
                .long("destination")
                .value_name("DESTINATION")
                .help("Define an output directory as the destination.")
                .takes_value(true))
        .get_matches();

    
}
